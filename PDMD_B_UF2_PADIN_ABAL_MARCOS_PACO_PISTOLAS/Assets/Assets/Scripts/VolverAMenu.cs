using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VolverAMenu : MonoBehaviour
{
    // Tiempo para cambiar de escena
    private float tiempoParaCambiar = 30.0f;

    // Start is called before the first frame update
    void Start()
    {
        // Inicia la cuenta atrás para cambiar de escena
        StartCoroutine(CuentaAtras());
    }

    // Update is called once per frame
    void Update()
    {
        // Comprueba si se ha pulsado la tecla 'e'
        if (Input.GetKeyDown(KeyCode.E))
        {
            // Cambia a la escena MainMenu
            SceneManager.LoadScene("MainMenu");
        }
    }

    IEnumerator CuentaAtras()
    {
        yield return new WaitForSeconds(tiempoParaCambiar);
        SceneManager.LoadScene("MainMenu");
    }
}

