using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
    // Referencias a los botones
    public Button botonStart;
    public Button botonSalir;

    // Start is called before the first frame update
    void Start()
    {
        // Añade los listeners a los botones
        botonStart.onClick.AddListener(CambiarASiguienteEscena);
        botonSalir.onClick.AddListener(SalirDelJuego);
    }

    // Método para cambiar a la escena MainMenu
    void CambiarASiguienteEscena()
    {
        SceneManager.LoadScene("MainMenu");
    }

    // Método para salir del juego
    void SalirDelJuego()
    {
        Application.Quit();
    }
}

