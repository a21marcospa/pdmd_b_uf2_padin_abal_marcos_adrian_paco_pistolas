using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    [SerializeField] AudioClip gruntSound; // Añade esta línea
    public AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    protected void Update()
    {

    }

    // Este método se llama cuando tu objeto trampa entra en contacto con otro objeto
    void OnCollisionEnter(Collision collision)
    {
        // Comprueba si el objeto con el que ha colisionado es el jugador
        if (collision.gameObject.tag == "Player")
        {
            // Si es el jugador, reproduce el sonido grunt
            AudioSource.PlayClipAtPoint(gruntSound, Camera.main.transform.position, 1);
        }
    }
}

