using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : Enemy
{
    public float initialDelay = 2f;
    public GameObject bulletPrefab;
    public float fireRate;
    public Transform shotSpawner;
    private float nextFire;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Shoot", initialDelay, fireRate);
        shotSpawner = transform.Find("ShotSpawner");
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if(targetDistance < 0){
            if(!facingRight){
                flip();
            }
        }
        else{
            if(facingRight){
                flip();
            }
        }

        /*if(Mathf.Abs(targetDistance) < attackDistance && Time.time > nextFire && shotSpawner != null){
            anim.SetTrigger("Shooting");
            nextFire = Time.time + fireRate;
        }*/
    }

    void Shoot(){
        Debug.Log("Shooting");
        GameObject tempBullet = Instantiate(bulletPrefab, shotSpawner.position, shotSpawner.rotation);
        if(!facingRight){
            tempBullet.transform.eulerAngles = new Vector3(0,0,180);
        }
        anim.SetTrigger("Shooting");
    }

    public void Shooting(){
        Debug.Log("Shooting!");
        GameObject tempBullet = Instantiate(bulletPrefab, shotSpawner.position, shotSpawner.rotation);
        if(!facingRight){
            tempBullet.transform.eulerAngles = new Vector3(0,0,180);
        }
        anim.ResetTrigger("Shooting");
    }
}
