using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayShop : MonoBehaviour
{
    public GameObject shopPanel;
    [SerializeField] AudioClip UpgradeSound;
    private void OnTriggerEnter2D(Collider2D other){
        if (other.gameObject.tag == "Player") { // Comprueba si el objeto en contacto tiene la etiqueta 'Player'
            Player player = other.GetComponent<Player>();
            if(player != null){
                AudioSource.PlayClipAtPoint(UpgradeSound, Camera.main.transform.position, 1); // Mover esta línea aquí
                player.canFire = false;
                shopPanel.SetActive(true);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D other){
        if (other.gameObject.tag == "Player") { // Comprueba si el objeto en contacto tiene la etiqueta 'Player'
            Player player = other.GetComponent<Player>();
            if(player != null){
                player.canFire = true;
                shopPanel.SetActive(false);
            }
        }
    }
}

