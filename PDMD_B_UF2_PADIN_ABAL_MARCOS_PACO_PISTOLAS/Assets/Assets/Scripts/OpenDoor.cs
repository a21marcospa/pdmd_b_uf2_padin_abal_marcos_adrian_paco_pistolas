using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpenDoor : MonoBehaviour
{
    public Text text;
    public string levelName;
    public AudioClip sound; 
    private bool inDoor = false;
    private AudioSource audioSource; 

    private void Start(){
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.volume = 1.5f;
        audioSource.clip = sound; 
    }

    private void OnTriggerEnter2D(Collider2D collision){
        if(collision.gameObject.CompareTag("Player")){
            text.gameObject.SetActive(true);
            inDoor = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision){
        text.gameObject.SetActive(false);
        inDoor = false;
    }

    private void Update(){
        if(inDoor && Input.GetKey("e")){
            audioSource.Play(); 
            StartCoroutine(LoadSceneAfterDelay(1f)); 
        }
    }

    IEnumerator LoadSceneAfterDelay(float delay){
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(levelName);
    }
}

