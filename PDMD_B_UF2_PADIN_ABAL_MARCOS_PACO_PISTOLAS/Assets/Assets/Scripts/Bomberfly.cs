using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bomberfly : Enemy
{
    public float walkDistance;
    private bool walk;
    //private bool attack = false;
    private bool soundPlayed;
    private bool attackSoundPlayed = false;
    [SerializeField] AudioClip HeliceSound;
    private AudioSource audioSource; // Añade esta línea

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>(); // Añade esta línea
    }

    // Update is called once per frame
    protected override void Update()
    {
        
        base.Update();
        if(Mathf.Abs(targetDistance) < attackDistance){
            if (!audioSource.isPlaying) { // Añade esta línea
                audioSource.PlayOneShot(HeliceSound, 1); // Añade esta línea
            }
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        } else if (audioSource.isPlaying) { // Añade esta línea
            audioSource.Stop(); // Añade esta línea
        }
    }
}

