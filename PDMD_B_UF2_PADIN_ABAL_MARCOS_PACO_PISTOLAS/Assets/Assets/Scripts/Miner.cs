using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Miner : Enemy
{
    public float walkDistance;
    private bool walk;
    private bool attack = false;
    [SerializeField] AudioClip MotorSound;
    [SerializeField] AudioClip AttackSound;
    private AudioSource audioSource;
    private bool isAttackSoundPlaying = false; // Nueva variable

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = MotorSound;
        audioSource.loop = true;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        anim.SetBool("Walk", walk);
        anim.SetBool("Attack", attack);

        if(Mathf.Abs(targetDistance) < walkDistance){
            walk = true;
            if (!audioSource.isPlaying) {
                audioSource.Play();
            }
        } else {
            if (audioSource.isPlaying) {
                audioSource.Stop();
            }
            walk = false;
        }

        if(Mathf.Abs(targetDistance) < attackDistance){
            attack = true;
            if (!isAttackSoundPlaying) { // Comprueba si el sonido de ataque ya se está reproduciendo
                AudioSource.PlayClipAtPoint(AttackSound, Camera.main.transform.position, 0.4f);
                isAttackSoundPlaying = true; // Cambia el estado a 'reproduciendo'
            }
        } else {
            isAttackSoundPlaying = false; // Si el jugador no está en posición de ataque, cambia el estado a 'no reproduciendo'
        }
        
        if(health <= 0){
            audioSource.Stop();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    
    }

    private void FixedUpdate(){
        if(walk && !attack){
            if(targetDistance < 0){
                rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
                if(!facingRight){
                    flip();
                }
            }
            else{
                rb2d.velocity = new Vector2(-speed, rb2d.velocity.y);
                if(facingRight){
                    flip();
                }
            }
        }
    }

    public void ResetAttack(){
        attack = false;
    }

    private void OnDisable(){
        
    }
}

