using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pikeman : Enemy
{
    public float walkDistance;
    private bool walk;
    private bool attack = false;
    private bool soundPlayed = false;
    private bool attackSoundPlayed = false;
    private bool hasEnteredScreen = false;
    [SerializeField] AudioClip EnemyGruntSound;
    [SerializeField] AudioClip KniveSound;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    protected override void Update()
    {
        
        base.Update();
        anim.SetBool("Walk", walk);
        anim.SetBool("Attack", attack);

        if(Mathf.Abs(targetDistance) < walkDistance){

            walk = true;
            if (!hasEnteredScreen) {
                hasEnteredScreen = true;
                soundPlayed = false;
            }
        }

        if(Mathf.Abs(targetDistance) < attackDistance){
            attack = true;
            walk = false;
            if (!attackSoundPlayed) {
                Invoke("PlayKniveSound", 0.3f);
                attackSoundPlayed = true;
            }
        }


    }

    private void FixedUpdate(){
        if(walk && !attack && !soundPlayed){
            AudioSource.PlayClipAtPoint(EnemyGruntSound, Camera.main.transform.position, 1);
            soundPlayed = true;
        }
        if(walk && !attack){
            if(targetDistance < 0){
                rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
                if(!facingRight){
                    flip();
                }
            }
            else{
                rb2d.velocity = new Vector2(-speed, rb2d.velocity.y);
                if(facingRight){
                    flip();
                }
            }
        }
    }

    public void ResetAttack(){
        attack = false;
        attackSoundPlayed = false;
    }

    void PlayKniveSound()
    {
        AudioSource.PlayClipAtPoint(KniveSound, Camera.main.transform.position, 0.3f);
    }
}






