using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBox : MonoBehaviour
{
    public int health;
    public int bombs;
    [SerializeField] AudioClip ReloadNadeSound;
    private void OnTriggerEnter2D(Collider2D other){
        Player player = other.GetComponent<Player>();
        if(player != null){
            AudioSource.PlayClipAtPoint(ReloadNadeSound, Camera.main.transform.position, 1);
            player.SetHealthAndBombs(health, bombs);
            Destroy(gameObject);
        }
    }
}